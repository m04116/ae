import { editorActionTypes as types } from '../constants';

export const loadInitialText = () => ({
    type: types.LOAD_TEXT
});

export const loadSynonyms = (word, parent) => ({
    type: types.LOAD_SYNONYMS,
    payload: {
        word,
        parent
    }
});

export const clearParentSynonyms = () => ({
    type: types.CLEAR_PARENT_AND_SYNONYMS
})