import React, { Component } from 'react';

import './ControlPanel.css';

const ControlPanel = ({ handleTextFormat, handleTextColor }) => (
    <div id="control-panel">
        <div id="format-actions">
            <button className="format-action" type="button" onClick={() => handleTextFormat('bold')}><b>B</b></button>
            <button className="format-action" type="button" onClick={() => handleTextFormat('italic')}><i>I</i></button>
            <button className="format-action" type="button" onClick={() => handleTextFormat('underline')}><u>U</u></button>
            <button className="color-button black-btn" type="button" onClick={() => handleTextColor('')}></button>
            <button className="color-button grey-btn" type="button" onClick={() => handleTextColor('grey')}></button>
            <button className="color-button blue-btn" type="button" onClick={() => handleTextColor('blue')}></button>
            <button className="color-button red-btn" type="button" onClick={() => handleTextColor('red')}></button>
        </div>
    </div>
)

export default ControlPanel;
