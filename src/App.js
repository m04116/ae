import React, { Component } from 'react';
import { connect } from 'react-redux';

import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import SynonymsZone from './synonyms-zone/SynonymsZone';
import { loadInitialText, loadSynonyms, clearParentSynonyms } from './actions/editor';

import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
        this.toggleTextFormat = this.toggleTextFormat.bind(this);
        this.handleTextColor = this.handleTextColor.bind(this);
        this.handleSynonym = this.handleSynonym.bind(this);
        this.replaceSynonym = this.replaceSynonym.bind(this);
    }

    componentDidMount() {
        this.props.loadInitialText();
    }

    getParent() {
        const selected = window.getSelection();
        if (selected && selected.rangeCount) {
            const range = selected.getRangeAt(0);
            if (range.commonAncestorContainer.parentNode.parentNode.parentNode.id === 'text-block') {
                const parent = range.commonAncestorContainer.parentElement;
                return parent;
            }
        }
        return null;
    }

    toggleTextFormat(format) {
        this.getParent() && this.getParent().classList ? this.getParent().classList.toggle(format) : null;
    }

    handleTextColor(color) {
        if (this.getParent()) {
            const classes = this.getParent().classList.value.split(' ');
            const colorClasses = ['grey', 'blue', 'red'];
            const textClasses = classes.filter(currentClass => !colorClasses.includes(currentClass)).join(' ');
            let newClasses = `${textClasses} ${color}`;
            if (!color && !textClasses) {
                this.getParent().removeAttribute("class");
                return;
            } else if (!color) {
                newClasses = textClasses;
            } else if (!textClasses) {
                newClasses = color;
            }
            this.getParent().className = newClasses;
        }
    }

    handleSynonym() {
        if (this.getParent()) {
            const selectedText = window.getSelection().toString();
            const parent = this.getParent();
            if (selectedText) {
                this.props.loadSynonyms(selectedText, parent);
            }
        } else {
        }
    }

    replaceSynonym(word) {
        const { parentNode, clearParentSynonyms } = this.props;
        if (parentNode) {
            parentNode.innerText = `${word} `;
            // clearParentSynonyms();
        }
    }

    render() {
        const { initialText, synonyms, clearParentSynonyms, parentNode } = this.props;
        return (
            <div className="App">
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <ControlPanel handleTextFormat={this.toggleTextFormat} handleTextColor={this.handleTextColor}/>
                    <FileZone text={initialText} handleSynonym={this.handleSynonym}/>
                    { !!parentNode
                        && <SynonymsZone
                                synonyms={synonyms}
                                replaceSynonym={this.replaceSynonym}
                                clearParentSynonyms={clearParentSynonyms}/>}
                </main>
            </div>
        );
    }
};

const mapStateToProps = ({ editor }) => ({
    initialText: editor.initialText,
    synonyms: editor.synonyms,
    parentNode: editor.parentNode
});

export default connect(mapStateToProps, { loadInitialText, loadSynonyms, clearParentSynonyms })(App);
