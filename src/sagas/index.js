import { all } from 'redux-saga/effects';

import editorSagas from './editor';

const defaultSaga = function*() {
    yield all([...editorSagas]);
};

export default defaultSaga;