import { call, put, takeLatest, select, delay } from 'redux-saga/effects';
import axios from 'axios';

import { editorActionTypes as types } from '../constants';
import getMockText from '../text.service';

const loadInitialText = function*() {
    try {
        const text = yield call(getMockText);
        yield delay(1000); // simulate API delay
        yield put({ type: types.LOAD_TEXT_SUSSESS, payload: text })
    } 
    catch (e) {
        yield put({ type: types.LOAD_TEXT_FAILURE, error: { message: e.message } });
    }
};

const loadSynonyms = function*(action) {
    try {
        const { word, parent } = action.payload;
        const response = yield call(axios, `https://api.datamuse.com/words?rel_syn=${word}`);
        const synonymsData = response.data;
        const synonymsArr = synonymsData.map(obj => obj.word);
        yield put({ type: types.LOAD_SYNONYMS_SUCCESS, payload: synonymsArr });
        yield put({ type: types.SET_PARENT_NODE, payload: parent })
    } catch (e) {
        yield put({ type: types.LOAD_SYNONYMS_FAILURE, error: { message: e.message } });
    }
}

export default [
    takeLatest(types.LOAD_TEXT, loadInitialText),
    takeLatest(types.LOAD_SYNONYMS, loadSynonyms)
];