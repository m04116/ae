import React from 'react';

import './SynonymsZone.css';

const SynonymsZone = ({ synonyms, replaceSynonym, clearParentSynonyms }) => {
    const renderSynonyms = synonyms.map((word, i) => (<p key={i} onClick={() => replaceSynonym(word)}>{word}</p>))
    return (
        <div className="synonyms">
            <div className="synonyms-main">
                <div className="synonyms-header">
                    <h2>Select synonym</h2>
                    <button onClick={clearParentSynonyms}>X</button>
                </div>
                <div className="synonyms-list">
                    {renderSynonyms}
                </div>
            </div>
        </div>
    )
};

export default SynonymsZone;