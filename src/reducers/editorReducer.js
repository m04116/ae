import { editorActionTypes as types } from '../constants';

const initialState = {
    initialText: '',
    synonyms: [],
    parentNode: null
};

const editorReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.LOAD_TEXT_SUSSESS:
            return Object.assign({}, state, { initialText: action.payload });
        
        case types.LOAD_SYNONYMS_SUCCESS:
            return Object.assign({}, state, { synonyms: action.payload });

        case types.SET_PARENT_NODE:
            return Object.assign({}, state, { parentNode: action.payload });

        case types.CLEAR_PARENT_AND_SYNONYMS:
            return Object.assign({}, state, { parentNode: null, synonyms: [] });

        default:
            return state;
    }
};

export default editorReducer;