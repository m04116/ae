import React from 'react';

import './FileZone.css';

const FileZone = ({ text, handleSynonym }) => {
    const renderText = text.split(' ').map((word, i) => (<span key={i}>{word} </span>));
    return (
        <div id="file-zone">
            <div id="file">
                <div id="text-block"
                    contentEditable="true"
                    suppressContentEditableWarning="true"
                    onSelect={handleSynonym}
                >
                    <div>{renderText}</div>
                </div>
            </div>
        </div>
    );
};

export default FileZone;
